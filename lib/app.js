import express from 'express';
import Logger from './helper/logger';

const app = express();
const config = {
    port: 8080
};

app.use(Logger.logRequest);

app.get('/', (req, res) => {
    res.send('Hallo Welt');
});

app.listen(config.port, () => console.log('Server running and listening on port: '+config.port));
